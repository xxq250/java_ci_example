
## 操作步骤11

#### 1. 在线修改信息 src/main/java/controller/HelloWorld.java 文件信息内容,如下图:

![](https://forge.educoder.net/api/attachments/1663370?raw=true)

#### 2. 修改成功后，会自动出发构建、自动测试、自动部署
![](https://forge.educoder.net/api/attachments/1663375?raw=true)


#### 3. 构建成功后，访问如下链接，即可查看效果
http://106.75.236.63:8080/demo/hello

![](https://forge.educoder.net/api/attachments/1663377?raw=true)
